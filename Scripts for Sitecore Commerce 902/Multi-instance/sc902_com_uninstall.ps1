#define parameters
Param(
	[string]$InstanceSuffix = 'Sc902',
	[string]$Prefix = 'sc902com',
	[string]$CommerceOpsSiteName = 'CommerceOps_',
	[string]$CommerceShopsSiteName = 'CommerceShops_',
	[string]$CommerceAuthoringSiteName = 'CommerceAuthoring_',
	[string]$CommerceMinionsSiteName = 'CommerceMinions_',
	[string]$SitecoreBizFxSiteName = 'SitecoreBizFx_',
	[string]$SitecoreIdentityServerSiteName = 'SitecoreIdentityServer_',
	[string]$SolrService = 'Solr_6.6.2',
	[string]$PathToSolr = 'D:\solr-6.6.2\',
	[string]$SqlServer = 'DESKTOP-XXXXXXXX',
	[string]$SqlAccount = 'sa',
	[string]$SqlPassword = 'password'
)
#Write-TaskHeader function modified from SIF
Function Write-TaskHeader {
    param(
        [Parameter(Mandatory=$true)]
        [string]$TaskName,
        [Parameter(Mandatory=$true)]
        [string]$TaskType
    )

    function StringFormat {
        param(
            [int]$length,
            [string]$value,
            [string]$prefix = '',
            [string]$postfix = '',
            [switch]$padright
        )

        # wraps string in spaces so we reduce length by two
        $length = $length - 2 #- $postfix.Length - $prefix.Length
        if($value.Length -gt $length){
            # Reduce to length - 4 for elipsis
            $value = $value.Substring(0, $length - 4) + '...'
        }

        $value = " $value "
        if($padright){
            $value = $value.PadRight($length, '*')
        } else {
            $value = $value.PadLeft($length, '*')
        }

        return $prefix + $value + $postfix
    }

    $actualWidth = (Get-Host).UI.RawUI.BufferSize.Width
    $width = $actualWidth - ($actualWidth % 2)
    $half = $width / 2

    $leftString = StringFormat -length $half -value $TaskName -prefix '[' -postfix ':'
    $rightString = StringFormat -length $half -value $TaskType -postfix ']' -padright

    $message = ($leftString + $rightString)
    Write-Host ''
    Write-Host $message -ForegroundColor 'Red'
}

Function Remove-Service{
	[CmdletBinding()]
	param(
		[string]$serviceName
	)
	if(Get-Service "My Service" -ErrorAction SilentlyContinue){
		sc.exe delete $serviceName
	}
}

Function Remove-Website{
	[CmdletBinding()]
	param(
		[string]$siteName		
	)

	$appCmd = "C:\windows\system32\inetsrv\appcmd.exe"
	& $appCmd delete site $siteName
}

Function Remove-AppPool{
	[CmdletBinding()]
	param(		
		[string]$appPoolName
	)

	$appCmd = "C:\windows\system32\inetsrv\appcmd.exe"
	& $appCmd delete apppool $appPoolName
}

#Stop Solr Service
Write-TaskHeader -TaskName "Solr Services" -TaskType "Stop"
Write-Host "Stopping solr service"
Stop-Service $SolrService -Force -ErrorAction stop
Write-Host "Solr service stopped successfully"

#Delete solr cores
Write-TaskHeader -TaskName "Solr Services" -TaskType "Delete Cores"
Write-Host "Deleting Solr Cores"
$pathToCores = "$pathToSolr\server\solr\${InstanceSuffix}*Scope"
Remove-Item $pathToCores -recurse -force -ErrorAction stop
Write-Host "Solr Cores deleted successfully"

#Remove Sites and App Pools from IIS
Write-TaskHeader -TaskName "Internet Information Services" -TaskType "Remove Websites"


Write-Host "Deleting Website $CommerceOpsSiteName$InstanceSuffix"
Remove-Website -siteName $CommerceOpsSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceShopsSiteName$InstanceSuffix"
Remove-Website -siteName $CommerceShopsSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceAuthoringSiteName$InstanceSuffix"
Remove-Website -siteName $CommerceAuthoringSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceMinionsSiteName$InstanceSuffix"
Remove-Website -siteName $CommerceMinionsSiteName$InstanceSuffix  -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $SitecoreBizFxSiteName$InstanceSuffix"
Remove-Website -siteName $SitecoreBizFxSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $SitecoreIdentityServerSiteName$InstanceSuffix"
Remove-Website -siteName $SitecoreIdentityServerSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"



Remove-AppPool -appPoolName $CommerceOpsSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceShopsSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceAuthoringSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceMinionsSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $SitecoreBizFxSiteName$InstanceSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $SitecoreIdentityServerSiteName$InstanceSuffix -ErrorAction silentlycontinue


Remove-Item C:\inetpub\wwwroot\$CommerceOpsSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceShopsSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceAuthoringSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceMinionsSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$SitecoreBizFxSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$SitecoreIdentityServerSiteName$InstanceSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"


Write-TaskHeader -TaskName "SQL Server" -TaskType "Drop Databases"
#Drop databases from SQL
Write-Host "Dropping databases from SQL server"
push-location
import-module sqlps

Write-Host $("Dropping database SitecoreCommerce$($InstanceSuffix)_Global")
$commerceDbPrefix = $("DROP DATABASE IF EXISTS [SitecoreCommerce$($InstanceSuffix)_Global]")
Write-Host $("Query: $($commerceDbPrefix)")
invoke-sqlcmd -ServerInstance $SqlServer -U $SqlAccount -P $SqlPassword -Query $commerceDbPrefix -ErrorAction silentlycontinue

Write-Host $("Dropping database [SitecoreCommerce$($InstanceSuffix)_SharedEnvironments]")
$sharedDbPrefix = $("DROP DATABASE IF EXISTS [SitecoreCommerce$($InstanceSuffix)_SharedEnvironments]")
Write-Host $("Query: $($sharedDbPrefix)")
invoke-sqlcmd -ServerInstance $SqlServer -U $SqlAccount -P $SqlPassword -Query $sharedDbPrefix -ErrorAction silentlycontinue


Write-Host "Databases dropped successfully"
pop-location

#Start Solr up again
Write-TaskHeader -TaskName "Solr Services" -TaskType "Start"
Start-Service $SolrService -ErrorAction stop
Write-Host "Solr Services restarted, environment is clean to reinstall"