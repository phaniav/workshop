$lastSuccessfulBuildDateTime = "%vh.minDateTimeForCheckingProjectUpdate%"

if (Test-Path "%vh.LastSuccessfulBuildDateTimeFilePath%")
{
 $lastSuccessfulBuildDateTime = Get-Content %vh.LastSuccessfulBuildDateTimeFilePath%
}

if (Test-Path ".\gulp-config.js")
{
 remove-Item .\gulp-config.js
}

$content = 'module.exports = function () {{
  var instanceRoot = "%vh.instanceRoot_CA%";
  var config = {{
    websiteRoot: instanceRoot + "\\Website",
    sitecoreConfigs: instanceRoot + "\\Website",
    licensePath: instanceRoot + "\\Data\\license.xml",
    solutionName: "VietHoang.Sitecore",
    buildConfiguration: "%vh.buildConfiguration%",
    runCleanBuilds: true,
    errorOnFail: %vh.errorOnFail%,
	MSDeployConf: {{
		MSDeployPublishMethod: "%vh.deployPublishMethod%",
		DeployIisAppPath: "%vh.deployIisAppPath_CA%",
		MSDeployServiceURL: "%vh.deployServiceUrl_CA%",
		UserName: "%vh.deployUserName%",
		Password: "%vh.deployPassword%",
		RetryAttemptsForDeployment: 5
	}},
    lastSuccessfulBuildDateTime: "{0}",
    isCD: false,
	forceToPublish: true
  }}
  return config;
}}' -f $lastSuccessfulBuildDateTime

new-item -path .\ -name gulp-config.js -type "file" -value $content

Get-Content .\gulp-config.js