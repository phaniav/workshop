if (Test-Path %vhs.source.folder%\GlobalAssemblyInfo.cs)
{
 remove-Item %vhs.source.folder%\GlobalAssemblyInfo.cs
}

$content = '
using System.Reflection;

[assembly: AssemblyCompany("VietHoang")]
[assembly: AssemblyCopyright("Copyright � VietHoang 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("{0}")]
[assembly: AssemblyInformationalVersion("{0}")]
[assembly: AssemblyFileVersion("{0}")]
' -f "%vhs.AssemblyVersion%"

new-item -path %vhs.source.folder%\ -name GlobalAssemblyInfo.cs -type "file" -value $content

Get-Content %vhs.source.folder%\GlobalAssemblyInfo.cs